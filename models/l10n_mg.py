# # -*- coding: utf-8 -*-
# # Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import models, fields, api


class res_partner(models.Model):

    _inherit = "res.partner"

    nif= fields.Char(string="NIF", size=32)
    stat=fields.Char(string="STAT", size=32)
    rcs=fields.Char(string="RCS", size=32)
    cin = fields.Char(string="CIN", size=32)
    date_cin = fields.Date(string="Date CIN")
    cnaps_id = fields.Char(string="Matricule CNaPS")
    medical_id = fields.Char(string="Matricule Medicale")
    secteur = fields.Char(string="Secteur")