# -*- coding: utf-8 -*-
{
    'name': 'Madagascar - Accounting',
    'version': '1.2',
    'category': 'Localization',
    # 'description': """
    #     This is the module to manage the accounting chart for Madagascar in Odoo.
    #     ========================================================================

    #     This module applies to companies based in Madagascar ..

    #     This localisation module creates the VAT taxes of type 'tax included' for purchases
    #     (it is notably required when you use the module 'hr_expense'). Beware that these
    #     'tax included' VAT taxes are not managed by the fiscal positions provided by this
    #     module (because it is complex to manage both 'tax excluded' and 'tax included'
    #     scenarios in fiscal positions).

    #     This localisation module doesn't properly handle the scenario when a France-mainland
    #     company sells services to a company based in the DOMs. We could manage it in the
    #     fiscal positions, but it would require to differentiate between 'product' VAT taxes
    #     and 'service' VAT taxes. We consider that it is too 'heavy' to have this by default
    #     in l10n_fr; companies that sell services to DOM-based companies should update the
    #     configuration of their taxes and fiscal positions manually.

    # """,
    'description': """
        l10 n MG
    """,    

    'depends': ['base_iban', 'account', 'base_vat'],
    'data': [
        'data/l10n_mg_chart_data.xml',
        'data/account_chart_template_data.xml',
        'views/l10n_mg_view.xml',
        'data/account_tax_data.xml',
        'data/account_fiscal_position_template_data.xml',
        # 'data/account_reconcile_model_template.xml',
        'data/account_chart_template_data.yml',
    ],
    'installable': True,
}